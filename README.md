# Rock-Paper-Scissors Game



## Description

This is a Rock Paper Scissors game implemented in Python using socket programming. The game server is hosted on an Ubuntu VM, and a Docker image has been created for the server. Nginx has also been added to the Docker image for load balancing.
## Installation

1. Set up an Ubuntu VM or any other Linux machine.
2. Update the system and install the required dependencies by running the following commands:
```
$ sudo apt-get update
$ sudo apt-get install vim
$ sudo apt-get install docker.io
$ sudo apt-get install docker-compose

```
3. Create a directory for the application and switch to it:
```
$ sudo mkdir app
$ cd app

```
4. Copy the following files into the app directory:


1. [server.py](https://gitlab.com/ijkhan22/distributedsystems/-/blob/main/server.py)
2. [game.py](https://gitlab.com/ijkhan22/distributedsystems/-/blob/main/game.py)
3. [Dockerfile](https://gitlab.com/ijkhan22/distributedsystems/-/blob/main/dockerfile)
4. [docker-compose.yml](https://gitlab.com/ijkhan22/distributedsystems/-/blob/main/docker-compose.yml)
5. [nginx.conf](https://gitlab.com/ijkhan22/distributedsystems/-/blob/main/nginx.conf)


6. (Optional) Edit the nginx.conf file to customize the Nginx settings.

## Deployment
1. Build and start the Docker containers by running the following command:
```
$ sudo docker-compose up -d --build
```
2. To scale up the number of game servers, use the following command:

```
$ sudo docker-compose up -d --scale app=5
```
## Playing the game
To play the game, follow these steps:

1. Install Python on your local machine if you haven't already.
2. Clone the repository from GitLab.
3. Navigate to the directory containing the [client.py](https://gitlab.com/ijkhan22/distributedsystems/-/blob/main/client.py) file.
4. Run the to play the game you need to run the [client.py](https://gitlab.com/ijkhan22/distributedsystems/-/blob/main/client.py) file
5. file using the following command:

```
$ python client.py
```
5. play the game
6. enjoy :)
Note: The game can be played from any machine and they don't need to be on the same computer.