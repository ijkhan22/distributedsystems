FROM python:3.9
WORKDIR /app
COPY ./server.py /app
COPY ./game.py /app
CMD ["bash" , "-c" ,"python server.py"]